const allGenresUrl = "http://localhost:51673/api/Genre/genres";
const borrowBookUrl = "http://localhost:51673/api/Book/borrow";
const changeBookTitleUrl = "http://localhost:51673/api/Book/book";
const getBooksByGenreIdUrl = "http://localhost:51673/api/Book/booksbygenres";

let genresList = document.querySelector("#genres");
let booksList = document.querySelector("#books");

const getAllGenres = async () => {
  try {
    return await axios.get(allGenresUrl);
  } catch (error) {
    console.log(error);
  }
};

const getBooksByGenreId = async (checkedGenres) => {
  try {
    return await axios.post(getBooksByGenreIdUrl, checkedGenres);
  } catch (error) {
    console.log(error);
  }
};

const editBookTitle = async (newTitle) => {
  try {
    return await axios.put(changeBookTitleUrl, newTitle);
  } catch (error) {
    console.log(error);
  }
};

function getCheckedGenres() {
  let checkBoxes = document.querySelectorAll("input[type=checkbox]:checked");
  let chkbxIds = [];

  for (let i = 0; i < checkBoxes.length; i++) {
    chkbxIds.push(checkBoxes[i].defaultValue);
  }
  let genres = {
    genreIds: chkbxIds,
  };

  return genres;
}

function renameBook() {
  let th = this.parentNode.parentNode;
  let bookTitle = th.querySelector(".bookTitle");
  let innerText = bookTitle.innerText;
  console.log(innerText);
  let input = document.createElement("input");
  input.value = innerText;

  th.replaceChild(input, bookTitle);

  let saveButton = document.createElement("button");
  let cancelButton = document.createElement("button");

  saveButton.innerText = "Save";
  saveButton.classList.add("btn");
  saveButton.classList.add("btn-primary");

  cancelButton.innerText = "Cancel";
  cancelButton.classList.add("btn");
  cancelButton.classList.add("btn-danger");

  cancelButton.addEventListener("click", function () {
    th.replaceChild(bookTitle, input);
    let edit = this.parentNode.querySelector(".btn.btn-success");
    let save = this.parentNode.querySelector(".btn.btn-primary");
    save.remove();
    this.remove();
    edit.style.display = "";
  });

  saveButton.addEventListener("click", function () {
    bookTitle.innerText = input.value;
    th.replaceChild(bookTitle, input);
    let edit = this.parentNode.querySelector(".btn.btn-success");
    let cancel = this.parentNode.querySelector(".btn.btn-danger");
    cancel.remove();
    this.remove();
    edit.style.display = "";
    let id = th.querySelector("th").innerHTML;

    let newTitle = {
      Id: id,
      Title: bookTitle.innerText,
    };

    editBookTitle(newTitle);
  });

  this.parentNode.appendChild(saveButton);
  this.parentNode.appendChild(cancelButton);

  this.style.display = "none";
}

const createBooksByGenreId = async () => {
  booksList.innerHTML = " ";
  let checkedGenres = getCheckedGenres();
  let response = await getBooksByGenreId(checkedGenres);
  let books = response.data;
  console.log(books);

  for (let i = 0; i < books.length; i++) {
    let th = document.createElement("th");
    let tr = document.createElement("tr");
    let tdBookTitle = document.createElement("td");
    tdBookTitle.classList.add("bookTitle");
    let tdAuthorName = document.createElement("td");
    let tdAuthorLastName = document.createElement("td");
    let tdEdit = document.createElement("td");
    let editButton = document.createElement("button");
    editButton.classList.add("btn");
    editButton.classList.add("btn-success");
    editButton.innerText = "Edit";
    editButton.addEventListener("click", renameBook);

    th.innerText = books[i].id;
    tdBookTitle.innerText = books[i].title;
    tdAuthorName.innerText = books[i].author.firstName;
    tdAuthorLastName.innerText = books[i].author.lastName;
    booksList.appendChild(tr);
    tr.appendChild(th);
    tr.appendChild(tdBookTitle);
    tr.appendChild(tdAuthorName);
    tr.appendChild(tdAuthorLastName);
    tr.appendChild(tdEdit);
    tdEdit.appendChild(editButton);
  }
};

const createGenresCheckboxes = async () => {
  let response = await getAllGenres();
  let genres = response.data;
  for (let i = 0; i < genres.length; i++) {
    let formCheck = document.createElement("div");
    formCheck.classList.add("form-check");
    formCheck.classList.add("col-md-3");
    formCheck.classList.add("mb-4");

    let check = document.createElement("div");
    check.classList.add("form-check");

    let chkbx = document.createElement("input");
    chkbx.classList.add("form-check-input");
    chkbx.setAttribute("type", "checkbox");
    chkbx.setAttribute("id", `genre${i}`);
    chkbx.setAttribute("value", genres[i].id);

    let label = document.createElement("label");
    label.classList.add("form-check-label");
    label.htmlFor = `genre${i}`;
    label.innerText = genres[i].name;

    genresList.appendChild(formCheck);
    formCheck.appendChild(check);

    check.appendChild(chkbx);
    check.appendChild(label);
  }
  let divCol12 = document.createElement("div");
  divCol12.classList.add("form-group");
  divCol12.classList.add("col-12");

  let searchButton = document.createElement("button");
  searchButton.addEventListener("click", createBooksByGenreId);
  searchButton.setAttribute("type", "button");
  searchButton.classList.add("btn");
  searchButton.classList.add("btn-primary");
  searchButton.classList.add("btn-lg");
  searchButton.innerHTML = "Search";
  genresList.appendChild(divCol12);
  divCol12.appendChild(searchButton);
};

createGenresCheckboxes();
