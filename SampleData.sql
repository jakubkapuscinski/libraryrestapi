INSERT	dbo.Genres
(
    --Id - column value is auto-generated
    Name
)
VALUES
(
    -- Id - int
    N'History' -- Name - nvarchar
),
(
    -- Id - int
    N'Horror' -- Name - nvarchar
),
(
    -- Id - int
    N'Children�s' -- Name - nvarchar
)
INSERT	INTO	dbo.Authors
(
    --Id - column value is auto-generated
    FirstName,
    LastName
)
VALUES
(
    -- Id - int
    N'J.K', -- FirstName - nvarchar
    N'Rowling' -- LastName - nvarchar
),
(
    -- Id - int
    N'E.B', -- FirstName - nvarchar
    N'White' -- LastName - nvarchar
),
(
    -- Id - int
    N'Margaret', -- FirstName - nvarchar
    N'Wise' -- LastName - nvarchar
),
(
    -- Id - int
    N'Bram', -- FirstName - nvarchar
    N'Stoker' -- LastName - nvarchar
),
(
    -- Id - int
    N'Stephen', -- FirstName - nvarchar
    N'King' -- LastName - nvarchar
),
(
    -- Id - int
    N'David', -- FirstName - nvarchar
    N' McCullough' -- LastName - nvarchar
)
INSERT	INTO dbo.Books
(
    --Id - column value is auto-generated
    Title,
    AuthorId,
    IsBorrowed,
    GenreId
)
VALUES
(
    -- Id - int
    N'Harry Potter and the Philosophers Stone', -- Title - nvarchar
    1, -- AuthorId - int
    0, -- IsBorrowed - bit
    3 -- GenreId - int
),
(
    -- Id - int
    N' Charlottes Web', -- Title - nvarchar
    2, -- AuthorId - int
    0, -- IsBorrowed - bit
    3 -- GenreId - int
),
(
    -- Id - int
    N' Goodnight Moon', -- Title - nvarchar
    3, -- AuthorId - int
    0, -- IsBorrowed - bit
    3 -- GenreId - int
),
(
    -- Id - int
    N'Drakula', -- Title - nvarchar
    4, -- AuthorId - int
    0, -- IsBorrowed - bit
    2 -- GenreId - int
),
(
    -- Id - int
    N'Carrie', -- Title - nvarchar
    5, -- AuthorId - int
    0, -- IsBorrowed - bit
    2 -- GenreId - int
),
(
    -- Id - int
    N'1776', -- Title - nvarchar
    6, -- AuthorId - int
    0, -- IsBorrowed - bit
    1 -- GenreId - int
)


