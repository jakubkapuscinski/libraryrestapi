﻿using AutoMapper;
using library_api.Core.Entities;
using library_app.Core.DTOs;

namespace library_app.Core.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Book, BookDTO>().ReverseMap();
            CreateMap<Author, AuthorWithBooksDTO>().ReverseMap();
        }
    }
}
