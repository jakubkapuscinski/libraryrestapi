﻿using System;
using System.Collections.Generic;

namespace library_api.Core.Entities
{
    public class Borrower : Entity
    {
        public string FirstName  { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public ICollection<Borrows> Borrows { get; set; }
    }
}
