﻿using library_app.Core.Entities;
using System.Collections.Generic;

namespace library_api.Core.Entities
{
    public class Book : Entity
    {
        public string Title { get; set; }
        public bool IsBorrowed { get; set; } = false;
        public Author Author { get; set; }
        public Genre Genre { get; set; }
        public ICollection<Borrows> Borrows { get; set; }
        public ICollection<Picture> Pictures { get; set; }
    }
}
