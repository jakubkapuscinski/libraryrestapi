﻿namespace library_api.Core.Entities
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}
