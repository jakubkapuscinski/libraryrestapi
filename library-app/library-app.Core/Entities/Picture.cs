﻿using library_api.Core.Entities;

namespace library_app.Core.Entities
{
    public class Picture: Entity
    {
        public string PathName { get; set; }
        public bool IsMiniature { get; set; }
        public Book Book { get; set; }
    }
}
