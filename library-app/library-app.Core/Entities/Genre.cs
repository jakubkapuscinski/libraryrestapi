﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace library_api.Core.Entities
{
    public class Genre : Entity
    {
        public string Name { get; set;}
        public ICollection<Book> Books { get; set; }
    }
}
