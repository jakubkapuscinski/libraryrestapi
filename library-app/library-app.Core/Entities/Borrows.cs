﻿using System;

namespace library_api.Core.Entities
{
    public class Borrows : Entity
    {
        public DateTime TakenDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public Borrower Borrower { get; set; }
        public Book Book { get; set; }
    }
}
