﻿using library_api.Core.Entities;
using library_app.Core.DTOs;
using library_app.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library_app.Core.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;

        public AuthorService(IAuthorRepository authorRepository)
        {
            _authorRepository = authorRepository;
        }

        public override async Task<AuthorWithBooksDTO> GetBooksByAuthorData(string Name, string LastName)
        {
            var authorWithBooks = await _authorRepository.GetBooksByAuthorFullName(Name, LastName);

            var bookTitlesWithIds = GetBooksTitlesWithIds(authorWithBooks);

            return bookTitlesWithIds;
        }

        public override async Task<AuthorWithBooksDTO> GetBooksByAuthorData(string Data)
        {
            var authorWithBooks = await _authorRepository.GetBooksByAuthorData(Data);

            var bookTitlesWithIds = GetBooksTitlesWithIds(authorWithBooks);

            return bookTitlesWithIds;

        }

        private AuthorWithBooksDTO GetBooksTitlesWithIds(Author authorWithBooks)
        {
            var booksTitlesWithIds = authorWithBooks.Books
                .Select(book => new List<string> { book.Id.ToString(), book.Title })
                .ToList();

            return new AuthorWithBooksDTO
            {
                Name = authorWithBooks.FirstName,
                LastName = authorWithBooks.LastName,
                BooksTitlesWithIds = booksTitlesWithIds,
            };
        }
    }
}

