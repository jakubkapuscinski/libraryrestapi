﻿using library_api.Core.Entities;
using library_app.Core.DTOs;
using library_app.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library_app.Core.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;

        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
        }
        
        public void Add(GenreDTO genreDTO)
        {
            Genre genre = new Genre
            {
                Name = genreDTO.Name
            };
            
            _genreRepository.Add(genre);
        }

        public  async Task<IEnumerable<Genre>> GetAllGenres()
        {
            var allGeneres = await _genreRepository.GetAllGenres();

            return allGeneres.ToList();

        }
    }
}
