﻿using library_api.Core.Entities;
using library_app.Core.DTOs;
using library_app.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library_app.Core.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;
        public BookService(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public async Task BorrowBookByBookIdAndUserId(int bookId, int borrowerId)
        {
            await _bookRepository.BorrowBookByBookIdAndUserId(bookId, borrowerId);
        }

        public async Task<BorrowedBookTitlesDTO> GetBorrowedBooks(int borrowerId)
        {
            var book = await _bookRepository.GetBorrowedBooks(borrowerId);

            var titlesList = book.Select(title => title.Title)
                .ToList();

            return new BorrowedBookTitlesDTO
            {
                Title = titlesList
            };
        }

        public async Task<IEnumerable<Book>> GetBooksByGenreId(List<string> genreId)
        {
            List<int> genreIds = new List<int>();
            foreach (var id in genreId)
            {
                var converted = Int32.Parse(id);
                genreIds.Add(converted);
            }

            var book = await _bookRepository.GetBooksByGenreId(genreIds);

            var books = book.ToList();

            return books;
        }

        public async Task UpdateBookTitle(int bookId, string bookTitle)
        {
            await _bookRepository.UpdateBookTitle(bookId, bookTitle);
        }

        public async Task<IEnumerable<Book>> GetAll()
        {
            var books = await _bookRepository.GetAll();

            return books.ToList();
        }
    }
}
