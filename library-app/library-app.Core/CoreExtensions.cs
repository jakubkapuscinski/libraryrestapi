﻿using AutoMapper;
using library_app.Core.AutoMapper;
using library_app.Core.Interfaces;
using library_app.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace library_app.Core
{
    public static class CoreExtensions
    {
        public static IServiceCollection AddCore(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();

            services.AddSingleton(mapper);
            services.AddScoped<IAuthorService, AuthorService>();
            services.AddScoped<IBookService, BookService>();
            services.AddScoped<IGenreService, GenreService>();

            return services;
        }
    }
}
