﻿namespace library_app.Core.DTOs
{
    public class AuthorDTO
    {
        public string Name { get; set; }
        public string LastName { get; set; }
    }
}
