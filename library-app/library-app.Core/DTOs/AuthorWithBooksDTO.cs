﻿using System.Collections.Generic;

namespace library_app.Core.DTOs
{
    public class AuthorWithBooksDTO : AuthorDTO
    {
        public List<List<string>> BooksTitlesWithIds { get; set; }
    }
}
