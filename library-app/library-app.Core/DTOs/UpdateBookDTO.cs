﻿namespace library_app.Core.DTOs
{
    public class UpdateBookDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
