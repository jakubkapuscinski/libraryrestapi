﻿namespace library_app.Core.DTOs
{
    public class GenreDTO
    {
        public string Name { get; set; }
    }
}
