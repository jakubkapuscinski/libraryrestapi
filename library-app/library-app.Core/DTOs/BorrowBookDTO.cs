﻿namespace library_app.Core.DTOs
{
    public class BorrowBookDTO
    {
        public int BookId { get; set; }
        public int BorrowerId { get; set; }
    }
}
