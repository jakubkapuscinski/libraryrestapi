﻿using System.Collections.Generic;

namespace library_app.Core.DTOs
{
    public class GenreIdsDTO
    {
        public  List<string> genreIds { get; set; }
    }
}
