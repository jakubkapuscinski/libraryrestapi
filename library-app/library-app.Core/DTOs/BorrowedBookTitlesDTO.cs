﻿using System.Collections.Generic;

namespace library_app.Core.DTOs
{
    public class BorrowedBookTitlesDTO
    {
        public List<string> Title { get; set; }
    }
}
