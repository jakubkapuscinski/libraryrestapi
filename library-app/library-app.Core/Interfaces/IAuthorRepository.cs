﻿using library_api.Core.Entities;
using System.Threading.Tasks;

namespace library_app.Core.Interfaces
{
    public interface IAuthorRepository
    {
        Task<Author> GetBooksByAuthorFullName(string name, string lastName);
        Task<Author> GetBooksByAuthorData(string data);
    }
}
