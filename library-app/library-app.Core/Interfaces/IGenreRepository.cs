﻿using library_api.Core.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace library_app.Core.Interfaces
{
    public interface IGenreRepository
    {
        void Add(Genre genre);
        Task<IQueryable<Genre>> GetAllGenres();
    }
}
