﻿using library_api.Core.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library_app.Core.Interfaces
{
    public interface IBookRepository
    {
        Task BorrowBookByBookIdAndUserId(int bookId,int borrowerId);
        Task<IQueryable<Book>> GetBorrowedBooks(int borrowerId);
        Task<IQueryable<Book>> GetBooksByGenreId(List<int> idsList);
        Task<IQueryable<Book>> GetAll();
        Task UpdateBookTitle(int bookId, string bookTitle);
    }
}
