﻿using library_app.Core.DTOs;
using System.Threading.Tasks;

namespace library_app.Core.Interfaces
{
    public abstract class IAuthorService
    {
        public abstract Task<AuthorWithBooksDTO> GetBooksByAuthorData(string Name, string LastName);
        public abstract Task<AuthorWithBooksDTO> GetBooksByAuthorData(string Data);

    }
}

