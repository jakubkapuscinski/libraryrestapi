﻿using library_api.Core.Entities;
using library_app.Core.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace library_app.Core.Interfaces
{
    public interface IBookService
    {
        Task BorrowBookByBookIdAndUserId(int bookId, int borrowerId);
        Task<BorrowedBookTitlesDTO> GetBorrowedBooks(int borrowerId);
        Task<IEnumerable<Book>> GetBooksByGenreId(List<string> genreId);    
        Task UpdateBookTitle(int bookId, string bookTitle);
        Task<IEnumerable<Book>> GetAll();
    }
}
