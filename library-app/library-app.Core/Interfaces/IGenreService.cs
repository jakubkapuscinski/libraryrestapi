﻿using library_api.Core.Entities;
using library_app.Core.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace library_app.Core.Interfaces
{
    public interface IGenreService
    {
        void Add(GenreDTO genre);
        Task<IEnumerable<Genre>> GetAllGenres();
    }
}
