﻿using library_api.Core.Entities;
using library_app.Core.Interfaces;
using library_app.Core.Services;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace Tests
{
    public class AuthorServiceTests
    {

        [Fact]
        public void WhenProvidedAuthorNameAndLastName_ThenValidationPass()
        {
            var author = new Author
            {
                FirstName = "Andrzej",
                LastName = "Sapkowski",
                Books = new List<Book> { new Book { Id = 1, Title = "Miecz Przeznaczenia" }, new Book { Id = 2, Title = "Pani Jeziora"}}
            };

            Mock<IAuthorRepository> authorRepositoryMock = new Mock<IAuthorRepository>();

            authorRepositoryMock.Setup(x => x.GetBooksByAuthorFullName("Andrzej", "Sapkowski")).ReturnsAsync(author);

            var sut = new AuthorService(authorRepositoryMock.Object);

            var result = sut.GetBooksByAuthorData("Andrzej", "Sapkowski");
        }

        [Fact]
        public void WhenProvidedOnlyAuthorName_ThenValidationPass()
        {
            var author = new Author
            {
                FirstName = "Andrzej",
                LastName = "Sapkowski",
                Books = new List<Book> { new Book { Id = 1, Title = "Miecz Przeznaczenia" }, new Book { Id = 2, Title = "Pani Jeziora" } }
            };

            Mock<IAuthorRepository> authorRepositoryMock = new Mock<IAuthorRepository>();

            authorRepositoryMock.Setup(x => x.GetBooksByAuthorData("Andrzej")).ReturnsAsync(author);

            var sut = new AuthorService(authorRepositoryMock.Object);

            var result = sut.GetBooksByAuthorData("Andrzej");
        }

        [Fact]
        public void WhenProvidedOnlyAuthorLastName_ThenValidationPass()
        {
            var author = new Author
            {
                FirstName = "Andrzej",
                LastName = "Sapkowski",
                Books = new List<Book> { new Book { Id = 1, Title = "Miecz Przeznaczenia" }, new Book { Id = 2, Title = "Pani Jeziora" } }
            };

            Mock<IAuthorRepository> authorRepositoryMock = new Mock<IAuthorRepository>();

            authorRepositoryMock.Setup(x => x.GetBooksByAuthorData("Sapkowski")).ReturnsAsync(author);

            var sut = new AuthorService(authorRepositoryMock.Object);

            var result = sut.GetBooksByAuthorData("Sapkowski");
        }
    }
}
