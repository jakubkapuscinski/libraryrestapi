﻿using library_app.API.Controllers;
using library_app.Core.Interfaces;
using Moq;
using System;
using Xunit;

namespace Tests
{
    public class AuthorControllerTest
    {
        [Fact]
        public async void WhenNeitherAuthorNameNorSurnameProvided_ThenValidationFails()
        {
            var name = "";
            var lastName = "";

            Mock<IAuthorService> authorService = new Mock<IAuthorService>();

            var sut = new AuthorController(authorService.Object);

            var result = sut.GetAuthorBooks(name, lastName);

            await Assert.ThrowsAsync<Exception>(async () => await result);
        }
    }

}
