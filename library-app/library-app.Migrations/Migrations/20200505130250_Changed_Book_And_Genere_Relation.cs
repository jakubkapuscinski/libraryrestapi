﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace library_app.Migrations.Migrations
{
    public partial class Changed_Book_And_Genere_Relation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Books_Generes_GenereId",
                table: "Books");

            migrationBuilder.DropIndex(
                name: "IX_Books_GenereId",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "GenereId",
                table: "Books");

            migrationBuilder.AddColumn<int>(
                name: "BookId",
                table: "Generes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Generes_BookId",
                table: "Generes",
                column: "BookId");

            migrationBuilder.AddForeignKey(
                name: "FK_Generes_Books_BookId",
                table: "Generes",
                column: "BookId",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Generes_Books_BookId",
                table: "Generes");

            migrationBuilder.DropIndex(
                name: "IX_Generes_BookId",
                table: "Generes");

            migrationBuilder.DropColumn(
                name: "BookId",
                table: "Generes");

            migrationBuilder.AddColumn<int>(
                name: "GenereId",
                table: "Books",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Books_GenereId",
                table: "Books",
                column: "GenereId");

            migrationBuilder.AddForeignKey(
                name: "FK_Books_Generes_GenereId",
                table: "Books",
                column: "GenereId",
                principalTable: "Generes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
