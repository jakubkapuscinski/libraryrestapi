﻿using library_api.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace library_app.Infrastructure.EF
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions<LibraryContext> options) : base(options)
        {

        }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Borrower> Borrowers { get; set; }
        public DbSet<Borrows> Borrows { get; set; }
        public DbSet<Genre> Genres{ get; set; }

    }
}
