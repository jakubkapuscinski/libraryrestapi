﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library_api.Core.Entities;
using library_app.Core.Interfaces;
using library_app.Infrastructure.EF;
using Microsoft.EntityFrameworkCore;

namespace library_app.Infrastructure.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly LibraryContext _context;

        public BookRepository(LibraryContext context)
        {
            _context = context;
        }

        public async Task BorrowBookByBookIdAndUserId(int bookId, int borrowerId)
        {

            Borrower borrower = await _context.FindAsync<Borrower>(borrowerId);

            if (borrower == null)
            {
                throw new Exception("Borrower doesn't exist");
            }

            Book book = await _context.FindAsync<Book>(bookId);

            if (book.IsBorrowed)
            {
                throw new Exception("Book is already borrowed");
            }

            book.IsBorrowed = true;

            var borrow = new Borrows
            {
                Book = book,
                Borrower = borrower,
                TakenDate = DateTime.Today,
                ReturnDate = DateTime.Today.AddDays(7)
            };

            _context.Add<Borrows>(borrow);
            await _context.SaveChangesAsync();
        }

        public async Task<IQueryable<Book>> GetBorrowedBooks(int borrowerId)
        {
            var books = _context.Borrows
                .Where(x => x.Borrower.Id == borrowerId)
                .Select(x => x.Book);

            return books;
        }

        public async Task<IQueryable<Book>> GetBooksByGenreId(List<int> genreIds)
        {
            var books = _context.Books
                .Where(x => genreIds.Contains(x.Genre.Id))
                .Include(x => x.Pictures)
                .Include(x => x.Author);

            return books;
        }

        public async Task<IQueryable<Book>> GetAll()
        {
            var books = _context.Books
                .Include(x => x.Author).ThenInclude(x => x.FirstName)
                .Include(x => x.Author).ThenInclude(x => x.LastName)
                .Include(x => x.Genre.Name);

            return books;
        }

        public async Task UpdateBookTitle(int bookId, string bookTitle)
        {
            var book = _context.Find<Book>(bookId);

            book.Title = bookTitle;

            await _context.SaveChangesAsync();
        }
    }
}
