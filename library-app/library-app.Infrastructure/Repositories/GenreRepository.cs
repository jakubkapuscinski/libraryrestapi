﻿using library_api.Core.Entities;
using library_app.Core.Interfaces;
using library_app.Infrastructure.EF;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace library_app.Infrastructure.Repositories
{
    public class GenreRepository : IGenreRepository
    {
        private readonly LibraryContext _context;

        public GenreRepository(LibraryContext context)
        {
            _context = context;
        }

        public void Add(Genre genre)
        {
            _context.Add(genre);
            _context.SaveChanges();
        }

        public async Task<IQueryable<Genre>> GetAllGenres()
        {
            var allGeneres = _context.Genres.Include(x => x.Books);

            return allGeneres;
        }
    }
}
