﻿using library_api.Core.Entities;
using library_app.Core.Interfaces;
using library_app.Infrastructure.EF;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace library_app.Infrastructure.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly LibraryContext _context;

        public AuthorRepository(LibraryContext context)
        {
            _context = context;
        }

        public async Task<Author> GetBooksByAuthorFullName(string name, string lastName)
        {
            var authorBooksByFullname = await _context.Authors.Include(x => x.Books)
                    .SingleOrDefaultAsync(x => x.LastName == lastName && x.FirstName == name);

            return authorBooksByFullname;
        }

        public async Task<Author> GetBooksByAuthorData(string data)
        {
            var authorBooks = await _context.Authors.Include(x => x.Books)
                   .SingleOrDefaultAsync(x => x.LastName == data || x.FirstName == data);

            return authorBooks;
        }
    }
}
