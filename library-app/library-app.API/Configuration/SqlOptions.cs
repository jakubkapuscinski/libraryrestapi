﻿namespace library_app.API.Configuration
{
    public class SqlOptions
    {
        public string ConnectionString { get; set; }
    }
}
