﻿using Microsoft.AspNetCore.Builder;

namespace library_app.API.Configuration
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseSwaggerDoc(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Library API");
                c.RoutePrefix = string.Empty;
            });

            return app;
        }
    }
}
