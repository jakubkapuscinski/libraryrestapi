﻿using library_app.Core.DTOs;
using library_app.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace library_app.API.Controllers
{
    [Route("api/[controller]")]
    public class GenreController : Controller
    {
        private readonly IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpPost()]
        public ActionResult AddGenere([FromBody] GenreDTO genreDTO)
        {
            _genreService.Add(genreDTO);

            return Ok();
        }

        [HttpGet("genres")]
        public ActionResult GetAllGenres()
        {
            return Ok(_genreService.GetAllGenres().Result);
        }
    }
}
