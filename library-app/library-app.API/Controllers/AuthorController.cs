﻿using library_app.Core.DTOs;
using library_app.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace library_app.API.Controllers
{
    [Route("api/[controller]")]
    public class AuthorController : Controller
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        [HttpGet("books")]
        public async Task<ActionResult<AuthorWithBooksDTO>> GetAuthorBooks(string name, string lastName)
        {
            if (string.IsNullOrEmpty((name + lastName)))
            {
                throw new Exception("You need to provide name or lastname of the Author");
            }
            if (string.IsNullOrEmpty(lastName))
            {
                return Ok(await _authorService.GetBooksByAuthorData(name));
            }
            if (string.IsNullOrEmpty(name))
            {
                return Ok(await _authorService.GetBooksByAuthorData(lastName));
            }
            return Ok(_authorService.GetBooksByAuthorData(name, lastName).Result);
        }
    }
}
