﻿using System;
using System.Threading.Tasks;
using library_api.Core.Entities;
using library_app.Core.DTOs;
using library_app.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace library_app.API
{
    [Route("api/[controller]")]
    public class BookController:Controller
    {
        private readonly IBookService _bookService;
        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpPost("borrow")]
        public  async Task<ActionResult> BorrowBook([FromBody] BorrowBookDTO bookDto)
        {
            try
            {
                await _bookService.BorrowBookByBookIdAndUserId(bookDto.BookId, bookDto.BorrowerId);
                return Ok("Borrowed");
            }
            catch (Exception e)
            {
                return Ok(e.Message);
            }
        }

        [HttpGet("borrows")]
        public async Task<ActionResult<BorrowedBookTitlesDTO>> GetBorrowerBooks(int borrowerId)
        {
            return Ok( _bookService.GetBorrowedBooks(borrowerId).Result);
        }

        [HttpGet("books")]
        public async Task<ActionResult<Book>> GetAll()
        {
            return Ok( _bookService.GetAll().Result);
        }

        [HttpPost("booksbygenres")]
        public async Task<ActionResult<BorrowedBookTitlesDTO>> GetBooksByGenreId([FromBody] GenreIdsDTO genreIds)
        {

            return Ok(_bookService.GetBooksByGenreId(genreIds.genreIds).Result);
        }

        [HttpPut("book")]
        public async Task<ActionResult<BorrowedBookTitlesDTO>> UpdateBook([FromBody] UpdateBookDTO bookDto)
        {

            return Ok(_bookService.UpdateBookTitle(bookDto.Id, bookDto.Title));
        }
    }
}
